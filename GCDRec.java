public class GCDRec {
    public static void main(String[] args) {

        System.out.println(GCDRec(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
    }

    public static int GCDRec(int number1, int number2) {

        if (number2 == 0) return number1;
        else return GCDRec(number2, number1 - ((number1 / number2) * number2));
    }
}
