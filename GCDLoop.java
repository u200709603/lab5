public class GCDLoop {
    public static void main(String[] args) {
        int number1, number2, q, r = 1;
        number1 = Integer.parseInt(args[0]);
        number2 = Integer.parseInt(args[1]);
        while (r != 0) {
            q = number1 / number2;
            r = number1 - (q * number2);
            number1 = number2;
            number2 = r;
        }
        System.out.println(number1);

    }
}
